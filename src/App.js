import React from 'react';

import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={process.env.PUBLIC_URL + '/imgs/omi.png'} className="omi" alt="Omi" />
        <img src={process.env.PUBLIC_URL + '/imgs/logo.png'} className="logo" alt="EnkO Box" />
        <img src={process.env.PUBLIC_URL + '/imgs/wirvsvirus.png'} className="wirvsvirus" alt="WirVsVirusLogo" />

      </header>
    </div>
  );
}

export default App;
